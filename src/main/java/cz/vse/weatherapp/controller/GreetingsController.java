package cz.vse.weatherapp.controller;

import cz.vse.weatherapp.service.CountingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.thymeleaf.util.StringUtils;

import java.util.Locale;

@Controller
@RequestMapping("/pozdrav")
public class GreetingsController {

    @Autowired
    private CountingService countingService;

    public GreetingsController() {

    }

    @GetMapping("/dobry-den")
    public String dobryDen(@RequestParam String jmeno, Model model) {
        countingService.pricti();
        model.addAttribute("name", jmeno);
        model.addAttribute("count", countingService.getPocet());
        return "pozdrav/dobryden";
    }



}
