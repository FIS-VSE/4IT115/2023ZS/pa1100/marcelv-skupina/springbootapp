package cz.vse.weatherapp.service;

import cz.vse.weatherapp.domain.rest.WeatherResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * responsible for making calls to the OpenWeatherMap API and
 * retrieving the current weather for a given city
 */
@Service
public class WeatherService {

    private static final String WEATHER_URL = "http://api.openweathermap.org/data/2.5/weather?q={city}&appid={apiKey}";

    private final RestTemplate restTemplate = new RestTemplate();

    @Autowired
    private Environment env;

    public String getCurrentWeather(String city) {
        String apiKey = env.getProperty("weather.api.key"); // get API key from application properties
        String url = WEATHER_URL.replace("{city}", city).replace("{apiKey}", apiKey);
        try {
            WeatherResponse response = restTemplate.getForObject(url, WeatherResponse.class);
            return response.getWeather().get(0).getDescription(); // return weather description
        } catch (Exception e) {
            return "Unable to fetch weather data"; // return error message if unable to fetch data
        }
    }

}
